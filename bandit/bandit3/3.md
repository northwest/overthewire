# bandit3

Okay, picking right up where we left off, we have level3. So connecting to the same host again, with the 
username `bandit2` and the password from the last challenge, we start.
```
$ls
spaces in this filename
```

So this is handled almost the same exact way as the challenge before, we want to open a file that has some 
special characters in it, so let's try the same method as before

```
$cat "spaces in this filename"
UmHadQclWmgdLOKQ3YNgjWxGoRMb5luK
```

Well, would you look at that!

Thank you, next

nwa
