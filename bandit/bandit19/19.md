# bandit19

Good day, nerds!

So we're tackling bandit19. Let's see what's up.

```
$ssh bandit18@localhost
```
*snip*
```
For support, questions or comments, contact us through IRC on
irc.overthewire.org #wargames.

Enjoy your stay!

Byebye !
Connection to localhost closed.

```
>The password for the next level is stored in a file readme in the homedirectory. Unfortunately, someone has modified .bashrc to log you out when you log in with SSH.

Well *fine* then! We'll do this differently. It looks like there's something funny going on in that .bashrc that disconnects us.

Fortunately ssh allows you to enter the commands you'd like to execute instead of dropping to shell, let's give it a shot!

```
$ssh bandit18@bandit.labs.overthewire.org -p2220 cat readme
This is a OverTheWire game server. More information on http://www.overthewire.org/wargames

bandit18@bandit.labs.overthewire.org's password:
IueksS7Ubh8G3DCwVzrTd8rAVOwq3M5x
```

Not too bad, right? 


Thanks for playing along,

nwa
